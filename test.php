<?php


function test_phash_one() {
    $ht = ph_dct_imagehash("/home/gzleo/dlnk/e0cc90d84f3dea05077b6680722_p2_mk4_s75X75_osd102d8.gif");
    echo $ht . "\n";
    var_dump($ht);

    $ht2 = ph_dct_imagehash("/home/gzleo/dlnk/e0cc90d84f3dea05077b6680722_p2_mk4_s75X100_osd102d8.gif");
    echo $ht2 . "\n";

    $ht3 = ph_dct_imagehash("/home/gzleo/dlnk/15224990.jpg");

    $idist = ph_image_dist($ht, $ht2);
    $idist2 = ph_image_dist($ht, $ht3);

    echo "dist: {$idist} {$idist2} \n";

    $sht = phash_toint($ht);
    $iht = (int)$sht;
    // $fiht = sprintf('%lu', $iht);
    echo "abc {$iht} \n";
}

function test_phash_many()
{
    $jpgs = glob("/home/gzleo/dlnk/*.jpg");
    $gifs = glob("/home/gzleo/dlnk/*.gif");
    $gifs = array();

    $imgs = array_merge($jpgs, $gifs);
    
    // print_r($imgs);
    $max_dist = 0;
    $min_dist = PHP_INT_MAX;
    $all_dists = array();
    $hash_time = 0.0;
    $dist_time = 0.0;

    for ($i = 0; $i < count($imgs)-1; $i++) {
        $btime = microtime(true);
        $ih1 = ph_dct_imagehash($imgs[$i]);
        // for ($j = $i+1; $j < count($imgs); $j ++) {
            $ih2 = ph_dct_imagehash($imgs[$i+1]);
            $etime = microtime(true);
            $dtime = $etime - $btime;
            $hash_time += $dtime;

            $btime = microtime(true);
            $idist = ph_image_dist($ih1, $ih2);
            $etime = microtime(true);
            $dtime = $etime - $btime;
            $dist_time += $dtime;

            echo "dist={$idist}, {$imgs[$i]} <=> {$imgs[$i+1]} \n";

            if ($idist > $max_dist) {
                $max_dist = $idist;
            }
            if ($idist < $min_dist) {
                $min_dist = $idist;
            }

            $nh1 = phash_toint($ih1);
            $nh2 = phash_toint($ih2);
            $all_dists[] = array('dist' => $idist,
                                 'img1' => $imgs[$i],
                                 'img2' => $imgs[$i+1],
                                 'ih1' => $nh1,
                                 'ih2' => $nh2,
                                 );
            // }
    }

    print_r($all_dists);
    echo "Max dist: {$max_dist}, Min dist: {$min_dist} \n";
    echo "Hash time: {$hash_time}, Dist time: {$dist_time} \n";
}

test_phash_many();

/*
    ulong64 x = hash1^hash2;
    const ulong64 m1  = 0x5555555555555555ULL;
    const ulong64 m2  = 0x3333333333333333ULL;
    const ulong64 h01 = 0x0101010101010101ULL;
    const ulong64 m4  = 0x0f0f0f0f0f0f0f0fULL;
    x -= (x >> 1) & m1;
    x = (x & m2) + ((x >> 2) & m2);
    x = (x + (x >> 4)) & m4;
    return (x * h01)>>56;

 */

/*
drop function phash_hamming_distance;
delimiter //
create function phash_hamming_distance(ih1 BIGINT, ih2 BIGINT)
RETURNS INT
BEGIN
    DECLARE dist INT;
DECLARE x BIGINT(22) UNSIGNED;
    DECLARE m1 BIGINT;
DECLARE m2 BIGINT;
DECLARE h01 BIGINT;
DECLARE m4 BIGINT;

SET m1 = 0x5555555555555555;
SET m2 = 0x3333333333333333;
SET h01 = 0x0101010101010101;
SET m4 = 0x0f0f0f0f0f0f0f0f;
SET dist = 6;

SET x = ih1 ^ ih2;
SET x = x - (x >> 1) & m1;    
SET x = (x & m2) + ((x >> 2) & m2);
SET x = (x + (x >> 4)) & m4;
SET x = (x * h01) >> 56;
    RETURN dist;
END
//
delimiter ;
select phash_hamming_distance(3412602822,1881928859);


*/





